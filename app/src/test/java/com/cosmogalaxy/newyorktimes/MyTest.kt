package com.cosmogalaxy.newyorktimes

import org.junit.Test

class MyTest {
    @Test
    fun getStories() {
        val x = arrayOf(1, 2, 3, 3,4, 5, 67, 43, 22, 45)
        val y = arrayOf(3, 5, 7, 33, 56, 22, 56, 3, 1)
        println(topArray(x, y))
    }

    fun topArray(x: Array<Int>, y: Array<Int>): MutableList<Int> {
        val result = mutableListOf<Int>()
        x.forEach {
            if (y.contains(it)) {
                result.add(it)
                y.drop(it)
            }
        }
        return result
    }
}