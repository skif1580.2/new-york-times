package com.cosmogalaxy.newyorktimes.utils

import androidx.recyclerview.widget.DiffUtil
import com.cosmogalaxy.newyorktimes.data.model.ItemArticle

class ItemInfoDiffCallback : DiffUtil.ItemCallback<ItemArticle>() {
    override fun areItemsTheSame(oldItem: ItemArticle, newItem: ItemArticle): Boolean =
        oldItem.title == newItem.title

    override fun areContentsTheSame(oldItem: ItemArticle, newItem: ItemArticle): Boolean =
        oldItem.title == newItem.title
}