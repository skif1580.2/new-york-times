package com.cosmogalaxy.newyorktimes.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cosmogalaxy.newyorktimes.data.model.Section
import com.cosmogalaxy.newyorktimes.domain.core.GetStoriesUseCase

class TopStoriesVM(private val getStoriesUseCase: GetStoriesUseCase) : ViewModel() {
    private var _topStories = MutableLiveData<UIResponseState>()

    val topStories: LiveData<UIResponseState>
        get() = _topStories


    fun getTopStories(section: String, key: String) {
        _topStories.postValue(UIResponseState.Loading)
        getStoriesUseCase.invoke(
            viewModelScope,
            getStoriesUseCase.Credentials(section, key)
        ) {
            it.handle(
                { e ->
                    _topStories.postValue(UIResponseState.Error(e.message ?: ""))
                },
                { result ->
                    _topStories.postValue(UIResponseState.Success(result))
                }
            )
        }
    }
}

sealed class UIResponseState {
    object Loading : UIResponseState()
    data class Error(val errorMessage: String) : UIResponseState()
    data class Success<T>(val content: T) : UIResponseState()
}