package com.cosmogalaxy.newyorktimes.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.cosmogalaxy.newyorktimes.R
import com.cosmogalaxy.newyorktimes.adapters.NewsAdapter
import com.cosmogalaxy.newyorktimes.data.model.ItemArticle
import com.cosmogalaxy.newyorktimes.databinding.FragmentTopStoriesBinding
import com.cosmogalaxy.newyorktimes.viewmodels.TopStoriesVM
import com.cosmogalaxy.newyorktimes.viewmodels.UIResponseState
import org.koin.android.viewmodel.ext.android.viewModel

class TopStories : Fragment() {
    private val storiesViewModel: TopStoriesVM by viewModel()
    private var _binding: FragmentTopStoriesBinding? = null
    private val binding get() = _binding!!
    private val itemAdapter: NewsAdapter = NewsAdapter().apply {
        clickListener {
            val bundle = ArticleFragmentArgs(it).toBundle()
            findNavController().navigate(R.id.action_topStories_to_articleFragment, bundle)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTopStoriesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        storiesViewModel.getTopStories("travel", getString(R.string.apiKey))
        binding.recycler.adapter = itemAdapter
        storiesViewModel.topStories.observe(viewLifecycleOwner, {
            when (it) {
                is UIResponseState.Loading -> {
                    binding.flProgress.visibility = VISIBLE
                }
                is UIResponseState.Success<*> -> {
                    showStateSuccess((it.content as List<ItemArticle>))
                }
                is UIResponseState.Error -> {
                    showStateError()
                }
            }
        })
    }

    private fun showStateSuccess(items: List<ItemArticle>) {
        binding.flProgress.visibility = GONE
        itemAdapter.submitList(items.toMutableList())
    }

    private fun showStateError() {
        binding.flProgress.visibility = GONE
        Toast.makeText(context, getString(R.string.error_message), Toast.LENGTH_SHORT)
            .show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}