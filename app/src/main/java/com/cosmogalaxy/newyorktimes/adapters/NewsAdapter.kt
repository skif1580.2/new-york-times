package com.cosmogalaxy.newyorktimes.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cosmogalaxy.newyorktimes.BR
import com.cosmogalaxy.newyorktimes.data.model.ItemArticle
import com.cosmogalaxy.newyorktimes.databinding.ItemRecyclerBinding
import com.cosmogalaxy.newyorktimes.utils.ItemInfoDiffCallback

class NewsAdapter : ListAdapter<ItemArticle, NewsAdapter.ItemViewHolder>(ItemInfoDiffCallback()) {
    private var clickListenerCallback: ((item: ItemArticle) -> Unit)? = null

    fun clickListener(po: (item: ItemArticle) -> Unit) {
        clickListenerCallback = po
    }


    class ItemViewHolder(private val binding: ItemRecyclerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bin(itemArticle: ItemArticle) {
            with(binding) {
                setVariable(BR.InfoNew, itemArticle)
                executePendingBindings()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemRecyclerBinding.inflate(layoutInflater, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bin(getItem(position))
        holder.itemView.setOnClickListener {
            clickListenerCallback?.invoke(getItem(position))
        }
    }

    override fun submitList(list: MutableList<ItemArticle>?) {
        super.submitList(list?.let { ArrayList(it) })
    }
}