package com.cosmogalaxy.newyorktimes.domain.core

sealed class Result<out F, out R> {

    data class Failure<out F>(val failure: F) : Result<F, Nothing>()

    data class Success<out R>(val result: R) : Result<Nothing, R>()

    val isSuccess get() = this is Success<R>

    val isFailure get() = this is Failure<F>

    fun handle(failureCallBack: (F) -> Any, successCallback: (R) -> Any): Any =
        when (this) {
            is Failure -> failureCallBack(failure)
            is Success -> successCallback(result)
        }
}
