package com.cosmogalaxy.newyorktimes.domain.core

import kotlinx.coroutines.*

abstract class UseCase<in Params, out Type> where Type : Any {

    abstract suspend fun run(params: Params): Result<Exception, Type>

    operator fun invoke(
        scope: CoroutineScope,
        params: Params,
        onResult: (Result<Exception, Type>) -> Unit = {}
    ) {
        val job = scope.async(Dispatchers.IO) { run(params) }
        GlobalScope.launch(Dispatchers.Main) { onResult(job.await()) }
    }

    class None
}