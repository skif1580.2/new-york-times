package com.cosmogalaxy.newyorktimes.domain.core

import com.cosmogalaxy.newyorktimes.data.model.ItemArticle
import com.cosmogalaxy.newyorktimes.data.repository.TravelRepository

class GetStoriesUseCase(private val repository: TravelRepository) :
    UseCase<GetStoriesUseCase.Credentials, List<ItemArticle>>() {

    inner class Credentials(val sections: String, val key: String)

    override suspend fun run(params: Credentials): Result<Exception, List<ItemArticle>> {
        return try {
            val result = repository.getTopStories(params.sections, params.key)
            Result.Success(result)
        } catch (e: Exception) {
            Result.Failure(e)
        }
    }
}
