package com.cosmogalaxy.newyorktimes.data.model

enum class Section(name: String) {
    TRAVEL("travel"),SPORTS("sports"),POLITICS("politics"),BOOKS("books"),SCIENCE("science")
}