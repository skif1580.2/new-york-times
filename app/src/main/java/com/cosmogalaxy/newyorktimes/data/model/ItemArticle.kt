package com.cosmogalaxy.newyorktimes.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ItemArticle(
    val section:String,
    val title: String,
    val image: String,
    val abstract:String,
) : Parcelable
