package com.cosmogalaxy.newyorktimes.data.network

import com.cosmogalaxy.newyorktimes.domain.network.response.TravelItemResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface StoriesApi {
    @GET("v2/{section}.json")
    suspend fun getTopTravelStories(@Path("section") section :String,@Query("api-key") apiKey:String)
    :TravelItemResponse
}