package com.cosmogalaxy.newyorktimes.data.retrofit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val baseUrl = "https://api.nytimes.com/svc/topstories/"

class RetrofitProvider {
    companion object {
        fun getRetrofit(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient())
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        }

        private val interceptor = HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)
        }

        private fun okHttpClient() =
            OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
    }
}