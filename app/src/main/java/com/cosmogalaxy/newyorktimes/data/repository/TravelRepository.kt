package com.cosmogalaxy.newyorktimes.data.repository

import com.cosmogalaxy.newyorktimes.data.model.ItemArticle
import com.cosmogalaxy.newyorktimes.data.network.StoriesApi
import com.cosmogalaxy.newyorktimes.domain.network.response.toItemArticle

class TravelRepository(private val api: StoriesApi) {

    suspend fun getTopStories(section:String,apiKey:String):List<ItemArticle>{
        return api.getTopTravelStories(section,apiKey).toItemArticle()
    }
}