package com.cosmogalaxy.newyorktimes.app

import android.app.Application
import com.cosmogalaxy.newyorktimes.di.appModule
import com.cosmogalaxy.newyorktimes.di.dataModule
import com.cosmogalaxy.newyorktimes.di.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@MyApp)
            modules(appModule, dataModule, domainModule)
        }
    }
}