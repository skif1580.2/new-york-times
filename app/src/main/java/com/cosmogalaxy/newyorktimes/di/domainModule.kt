package com.cosmogalaxy.newyorktimes.di

import com.cosmogalaxy.newyorktimes.domain.core.GetStoriesUseCase
import org.koin.dsl.module

val domainModule = module {
    single { GetStoriesUseCase(get()) }
}