package com.cosmogalaxy.newyorktimes.di

import com.cosmogalaxy.newyorktimes.viewmodels.TopStoriesVM
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule= module {
   viewModel { TopStoriesVM(get()) }
}