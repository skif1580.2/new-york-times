package com.cosmogalaxy.newyorktimes.di

import com.cosmogalaxy.newyorktimes.data.network.StoriesApi
import com.cosmogalaxy.newyorktimes.data.repository.TravelRepository
import com.cosmogalaxy.newyorktimes.data.retrofit.RetrofitProvider
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit

val dataModule= module {
    single { RetrofitProvider.getRetrofit() }
    single { get <Retrofit>().create(StoriesApi::class.java) }
    single { TravelRepository(get()) }
}